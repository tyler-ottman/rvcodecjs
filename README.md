# rvcodec.js

## Introduction

**rvcodec.js** is a RISC-V instruction encoder/decoder available at
<https://luplab.gitlab.io/rvcodecjs/>.

**rvcodec.js** conveniently shows how each token of the assembly instruction is
encoded as part of the binary representation.

## Contributing

### Run locally

- Clone this repo: `$ git clone git@gitlab.com:luplab/rvcodecjs.git`
- Install dependencies: `$ npm install`
- Make changes
    * The encoding/decoding logic is in directory `core`
    * The web user interface is in directory `web-ui`
- Run testsuite to avoid any regressions: `$ npm test`
- Open website locally to test UI: `$ npm run open`

### Roadmap

If you'd like to contribute, here are the main features we'd like to add:

- [x] Support for RV32I instruction set
- [ ] Support for RV64I instruction set
- [ ] Support for rest of GC extensions
    - [x] Zifencei instruction set
    - [ ] Zicsr instruction set
    - [ ] M (multiplication/division) instruction set
    - [ ] A (atomic) instruction set
    - [ ] F (single-precision floating point) instruction set
    - [ ] D (double-precision floating point) instruction set
    - [ ] C (compressed) instruction set

## Credit and license

**rvcodec.js** is developed by the [LupLab](https://luplab.cs.ucdavis.edu/) at
UC Davis. Contributors include or have included:

- Hikari Nicole Sakai *(backend's first functional version)*
- Abhiroop Sohal *(frontend's first function version)*
- Joël Porquet-Lupine

**rvcodec.js** is released under the [GNU Affero General Public License
v3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).

Zicsr notes:

- Most of the machine mode registers provided, supervisor and user level registers can easily be added
- Basic error checking provided (correct CSR or register names)


Tested Instructions (I used the RISCV GNU toolchain for rv32i at https://github.com/riscv-collab/riscv-gnu-toolchain):

csrrw x5, mtvec, x20
csrrc x6, mepc, x2
csrrs x0, mcause, x4
csrrwi x0, mie, 14
csrrsi x0, mip, 31
csrrci x25, mtval, 4

csrrw x40, mtvec, x20 (Incorrect register name)
csrrw x5, fakecsr, x20 (Incorrect csr name)
csrrci x25, mtval, 100 (Incorrect zimm field)